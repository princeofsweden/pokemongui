# PokemonGUI aka Pokedex or Pokdex

## Short Description
PokemnGUI is an Android App created in Android Studio. 
Its primary function  lies in extracting a large list of Pokemon and then
looking up more information about said Pokemon upon clicking on them in said list.

## How does it work
PokemonGUI calls the [PokeAPI](https://pokeapi.co/docs/v2.html) in two endpoints
via simple GET methods. Retrofit is in charge of the main connection logic 
that handles the REST calls via multiple callbacks.

The first endpoint is in charge of collecting all Pokemon from the
GET /pokemon-species/ endpoint to our main activity. 
We control how many Pokemon are collected via the built in limit parameter
in the API, in this case we set it to 900.

Continuing with that endpoint we put in a path parameter to specify the Pokemon
whose information we want to receive. All the information we would want about 
said Pokemon are specified in our model PokemonObjects. 
This information will then be used in our second activity.

However to complete all the needed information we also need to call the 
GET /pokemon/ endpoint. Similarly to the previous endpoint we input an ID to get
the remaining information we need. This endpoint is also in charge of getting
the Sprites that will be used in both activities.

## Tools we've used/ To run this project
We use [Project Lombok](https://projectlombok.org/setup/android) to generate getters, setters and other
constructors when needed via simple annotations. This makes for a much 
cleaner and more compact code as lombok will implement these methods when needed.

Unlike IntelliJ Android Studio doesn't work natively with lombok so to get
lombok to work you need download the lombook plugin in the plugin library.
Once downloaded you may need to restart Android Studio and then it should run
without any problem.

Other dependencies we've used for this project are Retrofit, Gson, 
RecyclerView, Glide and to some extent Room.

## Unfinished Code Snippets

There are some unfinished/ unused code in this project. Most of it being the
Sqlite implementation via Room. As the SqliteDB, unfortunately, was worked on
at the very end instead of at the start, it proved difficult to implement 
with the rest of the code without a serious overhaul. 

Originally the way the finished design was supposed to work was saving all of
the JsonObjects to the DB as a way of caching them, this way we could avoid
hitting the API limit in the PokemonApi while also speeding up the App.