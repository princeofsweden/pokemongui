package com.example.pokdex;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;
import com.example.pokdex.firstpage.JsonHolder;
import com.example.pokdex.models.GeneralObject;
import com.example.pokdex.models.PokemonObjects;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PokemonDetailsActivity extends AppCompatActivity {

    private DecimalFormat idPattern = new DecimalFormat("000");
    private DecimalFormat bodyPattern = new DecimalFormat("0.00");
    private static final String hashTag = "#";

    private static int bColor;
    private static int color;

    private final String TAG = "PokActivity";
    private ConstraintLayout pokemonInfoHeader;

    private TextView pokemonNameTextView;
    private TextView pokemonIdTextView;

    private TextView pokemonShapeTextView;
    private TextView pokemonHabitatTextView;
    private TextView pokemonGenerationTextView;
    private TextView pokemonhHasFormTextView;
    private TextView pokemonInfoTextView;
    private TextView pokemonHasGenusTextView;
    private TextView pokemonWeightTextView;
    private TextView pokemonHeightTextView;
    private TextView pokemonTypesTextView;
    private TextView pokemonColorTextView;
    private TextView evolvesFrom;
    private ImageView pokemonSprite;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pokemon_detail_new);
        pokemonNameTextView = findViewById(R.id.pokemonName);
        pokemonIdTextView = findViewById(R.id.textViewId);
        pokemonInfoHeader = findViewById(R.id.pokemonInfoHeaderLayout);
        pokemonShapeTextView = findViewById(R.id.pokemonShape);
        pokemonHabitatTextView = findViewById(R.id.pokemonHabitat);
        pokemonGenerationTextView = findViewById(R.id.pokemonGeneration);

        pokemonhHasFormTextView = findViewById(R.id.hasForm);
        pokemonHasGenusTextView = findViewById(R.id.hasGenus);
        pokemonInfoTextView = findViewById(R.id.pokemonInfo);

        pokemonWeightTextView = findViewById(R.id.pokemonWeight);
        pokemonHeightTextView = findViewById(R.id.pokemonHeight);
        pokemonTypesTextView = findViewById(R.id.pokemonType);
        pokemonColorTextView = findViewById(R.id.pokemonColor);
        evolvesFrom = findViewById(R.id.evolvesName);

        pokemonSprite = findViewById(R.id.pokemonSprite);


        final String pokemonId = Objects.requireNonNull(getIntent().getExtras()).getString("pokemonId");
        final String pokemonName = Objects.requireNonNull(getIntent().getExtras()).getString("pokemonName");

        assert pokemonId != null;
        getPokemonSpeciesInfo(Integer.valueOf(pokemonId));
        String pokeId = hashTag + idPattern.format(Integer.valueOf(pokemonId));
        pokemonNameTextView.setText(Objects.requireNonNull(pokemonName).toUpperCase());
        pokemonIdTextView.setText("DEX NR: " + pokeId);

    }

    public void getPokemonSpeciesInfo(Integer pokemonId) {
        Call<PokemonObjects> call = getJson().getPokemonSpeciesInfo(pokemonId);
        call.enqueue(new Callback<PokemonObjects>() {
            @Override
            public void onResponse(Call<PokemonObjects> call, Response<PokemonObjects> response) {
                if (!response.isSuccessful()) {
                    Log.d(TAG, "Error: " + response.errorBody());
                }

                Log.d(TAG, response.toString());

                assert response.body() != null;
                PokemonObjects info = response.body();

                String shape = info.getShape().getName();
                String habitat = info.getHabitat() == null? "N/A" : info.getHabitat().getName();
                String generation = info.getGeneration().getName();
                String hasGenus = hasNoGenus(info) ? "" : info.getGenera().get(2).getGenus();
                String infoText = getEnglishInfo(info.getFlavor_text_entries());
                String pokemonColor = info.getColor().getName();

                ConstraintLayout pokemonNameLayout = findViewById(R.id.pokemonInfoHeaderLayout);
                CardView basicInfoCard = findViewById(R.id.basicInfoCard);
                switch (info.getColor().getName()) {
                    case "white":
                        color = Color.argb(140, 255, 255, 0);
                        break;
                    case "brown":
                        color = Color.argb(140, 255, 250, 250);
                        break;
                    case "yellow":
                        color = Color.argb(140, 255, 255, 102);
                        break;
                    case "pink":
                        color = Color.argb(140, 255, 105, 180);
                        break;
                    default:
                        int intColor = Color.parseColor(info.getColor().getName());
                        String hexColor = String.format("#%06X", (0xFFFFFF & intColor));
                        Log.d(TAG, hexColor);
                        int r = Integer.valueOf(hexColor.substring(1, 3), 16);
                        int g = Integer.valueOf(hexColor.substring(3, 5), 16);
                        int b = Integer.valueOf(hexColor.substring(5, 7), 16);
                        color = Color.argb(100, r, g, b);
                        bColor = Color.argb(80, r, g, b);
                        break;
                }
                basicInfoCard.setCardBackgroundColor(color);
                pokemonNameLayout.setElevation(3);

                getOtherPokemonInfo(pokemonId);
                pokemonShapeTextView.setText(shape.toUpperCase());

                pokemonHabitatTextView.setText(habitat.toUpperCase());

                pokemonGenerationTextView.setText(generation.substring(11).toUpperCase());
                pokemonInfoTextView.setText(infoText);
                evolvesFrom.setText(hasGenus.replace("Pokémon", "").toUpperCase());
                Log.d(TAG, hasGenus);
                pokemonColorTextView.setText(pokemonColor.toUpperCase());

            }

            @Override
            public void onFailure(Call<PokemonObjects> call, Throwable t) {
                t.printStackTrace();
                call.cancel();

            }
        });
    }

    public void getOtherPokemonInfo(Integer id) {
        Call<PokemonObjects> call = getJson().getOtherPokemonInfo(id);
        call.enqueue(new Callback<PokemonObjects>() {
            @Override
            public void onResponse(Call<PokemonObjects> call, Response<PokemonObjects> response) {
                if (!response.isSuccessful()) {
                    Log.d(TAG, "Error: " + response.errorBody());
                }

                Log.d(TAG, response.toString());

                assert response.body() != null;
                PokemonObjects otherInfo = response.body();

                String spriteURL = otherInfo.getSprites().getFront_default();
                String types = hasTypes(otherInfo);
                double dWeight = otherInfo.getWeight() * 0.1;
                double dHeight = otherInfo.getHeight() / 10.0;
                String weight = bodyPattern.format(dWeight) + " KG";
                String height = bodyPattern.format(dHeight) + " M";

                Log.d(TAG, spriteURL);
                Log.d(TAG, types);
                Log.d(TAG, weight);
                Log.d(TAG, height);

                pokemonHeightTextView.setText(height);
                pokemonWeightTextView.setText(weight);
                pokemonTypesTextView.setText(hasTypes(otherInfo).toUpperCase());

                Glide.with(PokemonDetailsActivity.this).load(spriteURL).into(pokemonSprite);

            }

            @Override
            public void onFailure(Call<PokemonObjects> call, Throwable t) {
                t.printStackTrace();
                call.cancel();

            }
        });
    }

    //Helper methods

    private String hasTypes(PokemonObjects pok) {
        if (pok.getTypes().size() == 2) {
            return pok.getTypes().get(0).getType().getName() + "," +
                    " " + pok.getTypes().get(1).getType().getName();
        }

        return pok.getTypes().get(0).getType().getName();

    }

    private String getEnglishInfo(List<GeneralObject> pokeInfo) {
        String getEnglish = "";
        for (GeneralObject pok : pokeInfo) {
            if (pok.getLanguage().get("name").getAsString().equalsIgnoreCase("en")) {
                getEnglish = pok.getFlavor_text().replaceAll("\\n", " ");
            }
        }
        return getEnglish;
    }

    private boolean hasNoDescription(PokemonObjects pokemon) {
        return pokemon.getForm_descriptions().size() == 0;
    }

    private boolean hasNoGenus(PokemonObjects pokemon) {
        return pokemon.getGenera().size() == 0;
    }

    private JsonHolder getJson() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(JsonHolder.class);
    }

    public void showShape(View view) {
        pokemonShapeTextView.setVisibility(View.VISIBLE);
        pokemonHabitatTextView.setVisibility(View.GONE);
        pokemonGenerationTextView.setVisibility(View.GONE);
        pokemonhHasFormTextView.setVisibility(View.GONE);
        pokemonInfoTextView.setVisibility(View.GONE);
        pokemonHasGenusTextView.setVisibility(View.GONE);

    }

    public void showHabitat(View view) {
        pokemonShapeTextView.setVisibility(View.GONE);
        pokemonHabitatTextView.setVisibility(View.VISIBLE);
        pokemonGenerationTextView.setVisibility(View.GONE);
        pokemonhHasFormTextView.setVisibility(View.GONE);
        pokemonInfoTextView.setVisibility(View.GONE);
        pokemonHasGenusTextView.setVisibility(View.GONE);
    }

    public void showGeneration(View view) {
        pokemonShapeTextView.setVisibility(View.GONE);
        pokemonHabitatTextView.setVisibility(View.GONE);
        pokemonGenerationTextView.setVisibility(View.VISIBLE);
        pokemonhHasFormTextView.setVisibility(View.GONE);
        pokemonInfoTextView.setVisibility(View.GONE);
        pokemonHasGenusTextView.setVisibility(View.GONE);
    }

    public void showForm(View view) {
        pokemonShapeTextView.setVisibility(View.GONE);
        pokemonHabitatTextView.setVisibility(View.GONE);
        pokemonGenerationTextView.setVisibility(View.GONE);
        pokemonhHasFormTextView.setVisibility(View.VISIBLE);
        pokemonInfoTextView.setVisibility(View.GONE);
        pokemonHasGenusTextView.setVisibility(View.GONE);
    }

    public void showGenus(View view) {
        pokemonShapeTextView.setVisibility(View.GONE);
        pokemonHabitatTextView.setVisibility(View.GONE);
        pokemonGenerationTextView.setVisibility(View.GONE);
        pokemonhHasFormTextView.setVisibility(View.GONE);
        pokemonInfoTextView.setVisibility(View.GONE);
        pokemonHasGenusTextView.setVisibility(View.VISIBLE);
    }

    public void showInfo(View view) {
        pokemonShapeTextView.setVisibility(View.GONE);
        pokemonHabitatTextView.setVisibility(View.GONE);
        pokemonGenerationTextView.setVisibility(View.GONE);
        pokemonhHasFormTextView.setVisibility(View.GONE);
        pokemonInfoTextView.setVisibility(View.VISIBLE);
        pokemonHasGenusTextView.setVisibility(View.GONE);
    }
}