package com.example.pokdex.models;

import java.util.List;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class ResultsContainer {
    private final List<ResultModel> results;
}
