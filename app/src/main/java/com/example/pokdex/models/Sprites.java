package com.example.pokdex.models;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Sprites {

    private final String back_female;
    private final String back_shiny_female;
    private final String back_default;
    private final String front_female;
    private final String front_shiny_female;
    private final String back_shiny;
    private final String front_default;
    private final String front_shiny;

    }
