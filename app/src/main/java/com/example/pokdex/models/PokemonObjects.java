package com.example.pokdex.models;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PokemonObjects {

    private int id;
    private String name;
    private List<GeneralObject> types;
    private Sprites sprites;
    private Integer height;
    private Integer weight;
    private ResultModel color;
    private ResultModel shape;
    private ResultModel evolves_from_species;
    private ResultModel habitat;
    private ResultModel generation;
    private List<GeneralObject> flavor_text_entries;
    private List<GeneralObject> form_descriptions;
    private List<GeneralObject> genera;




}
