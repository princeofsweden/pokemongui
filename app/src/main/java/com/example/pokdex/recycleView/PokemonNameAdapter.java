package com.example.pokdex.recycleView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.pokdex.PokemonDetailsActivity;
import com.example.pokdex.R;
import com.example.pokdex.firstpage.JsonHolder;
import com.example.pokdex.models.PokemonObjects;
import com.example.pokdex.models.ResultModel;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class PokemonNameAdapter extends RecyclerView.Adapter<PokemonNameAdapter.PokemonViewHolder>
implements Filterable {

    private final List<PokemonObjects> resultList;
    private Context context;
    private DecimalFormat format = new DecimalFormat("000");
    private static final String hashTag = "#";
    private static int color;

    public PokemonNameAdapter(Context context, List<PokemonObjects> resultList) {
        this.resultList = resultList;
        this.context = context;
    }

    @NonNull
    @Override
    public PokemonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View mItemView = layoutInflater.inflate(R.layout.pokemon_name_list_item, parent, false);
        return new PokemonViewHolder(mItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final PokemonViewHolder holder, int position) {
        final PokemonObjects pokemon = resultList.get(position);
        String pokeId = hashTag + format.format(pokemon.getId());
        holder.PokemonNameItemView.setText(pokemon.getName());
        holder.PokemonIDItemView.setText(pokeId);

        Call<PokemonObjects> call = getJson().getPokemonSpeciesInfo(++position);
        call.enqueue(new Callback<PokemonObjects>() {
            @Override
            public void onResponse(Call<PokemonObjects> call, Response<PokemonObjects> response) {
                if (!response.isSuccessful()) {
                    System.out.println(response.errorBody());
                }
                assert response.body() != null;
                PokemonObjects info = response.body();
                switch (info.getColor().getName()) {
                    case "white":
                        color = Color.argb(140, 255, 255, 0);
                        break;
                    case "brown":
                        color = Color.argb(140, 255, 250, 250);
                        break;
                    case "yellow":
                        color = Color.argb(140, 255, 255, 102);
                        break;
                    case "pink":
                        color = Color.argb(140, 255, 105, 180);
                        break;
                    default:
                        int intColor = Color.parseColor(info.getColor().getName());
                        String hexColor = String.format("#%06X", (0xFFFFFF & intColor));
                        System.out.println(hexColor);
                        int r = Integer.valueOf(hexColor.substring(1, 3), 16);
                        int g = Integer.valueOf(hexColor.substring(3, 5), 16);
                        int b = Integer.valueOf(hexColor.substring(5, 7), 16);
                        color = Color.argb(100, r, g, b);
                        break;
                }
                ResultModel model = new ResultModel(info.getColor().getName(), info.getColor().getUrl());
                pokemon.setColor(model);
                Drawable background = holder.pokemonNameParentLayout.getBackground();
                if (background instanceof ShapeDrawable) {
                    // cast to 'ShapeDrawable'
                    ShapeDrawable shapeDrawable = (ShapeDrawable) background;
                    shapeDrawable.getPaint().setColor(color);
                } else if (background instanceof GradientDrawable) {
                    // cast to 'GradientDrawable'
                    GradientDrawable gradientDrawable = (GradientDrawable) background;
                    gradientDrawable.setColor(color);
                } else if (background instanceof ColorDrawable) {
                    // alpha value may need to be set again after this call
                    ColorDrawable colorDrawable = (ColorDrawable) background;
                    colorDrawable.setColor(color);
                } else {
                    ColorDrawable colorDrawable = (ColorDrawable) background;
                    colorDrawable.setColor(color);
                }
                holder.pokemonNameParentLayout.setElevation(3);
            }


            @Override
            public void onFailure(Call<PokemonObjects> call, Throwable t) {
                t.printStackTrace();
                call.cancel();

            }
        });

        Call<PokemonObjects> call1 = getJson().getOtherPokemonInfo(position);
        call1.enqueue(new Callback<PokemonObjects>() {
            @Override
            public void onResponse(Call<PokemonObjects> call, Response<PokemonObjects> response) {
                if (!response.isSuccessful()) {
                    Log.d(TAG, "Error: " + response.errorBody());
                }

                Log.d(TAG, response.toString());

                assert response.body() != null;
                PokemonObjects otherInfo = response.body();

                String spriteURL = otherInfo.getSprites().getFront_default();


                Glide.with(context).load(spriteURL).into(holder.imageView);

            }

            @Override
            public void onFailure(Call<PokemonObjects> call, Throwable t) {
                t.printStackTrace();
                call.cancel();

            }
        });


        holder.pokemonNameParentLayout.setOnClickListener(v -> {
            Toast.makeText(context, pokemon.getName(), Toast.LENGTH_LONG).show();
            Intent intent = new Intent(context, PokemonDetailsActivity.class);
            intent.putExtra("pokemonId", String.valueOf(pokemon.getId()));
            intent.putExtra("pokemonName", pokemon.getName());
            intent.putExtra("ookemonColor", pokemon.getColor().getName());
            context.startActivity(intent);
        });
    }

    private JsonHolder getJson() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(JsonHolder.class);
    }


    @Override
    public int getItemCount() {
        return resultList.size();
    }

    @Override
    public Filter getFilter() {
        return pokeFilter;
    }

    private Filter pokeFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<PokemonObjects> filteredList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(resultList);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (PokemonObjects pok : resultList) {
                    if (pok.getName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(pok);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            resultList.clear();
            resultList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };


    class PokemonViewHolder extends RecyclerView.ViewHolder {
        LinearLayout pokemonNameParentLayout;
        final TextView PokemonNameItemView;
        final TextView PokemonIDItemView;
        final ImageView imageView;

        //final PokemonNameAdapter mAdapter;
        PokemonViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.spriteView);
            PokemonNameItemView = itemView.findViewById(R.id.resultListNameTextView);
            PokemonIDItemView = itemView.findViewById(R.id.resultListIDTextView);
            pokemonNameParentLayout = itemView.findViewById(R.id.pokemonNameLayout);

        }

    }


}
