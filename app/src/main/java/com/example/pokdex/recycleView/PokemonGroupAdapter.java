package com.example.pokdex.recycleView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pokdex.R;


import java.util.List;

public class PokemonGroupAdapter extends  RecyclerView.Adapter<PokemonGroupAdapter.PokemonGroupViewHolder> {

    private final List<String> resultList;
    private Context context;


    public PokemonGroupAdapter(List<String> resultList, Context context) {
        this.resultList = resultList;
        this.context = context;
    }
    @NonNull
    @Override
    public PokemonGroupViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.pokemon_group_list,parent,false);
        return new PokemonGroupViewHolder(view,this);
    }

    @Override
    public void onBindViewHolder(@NonNull PokemonGroupViewHolder holder, int position) {
        final String mCurrent = resultList.get(position);
        holder.PokemonItemView.setText(mCurrent);
        holder.pokemonGroupParentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, mCurrent, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return resultList.size();
    }

    class PokemonGroupViewHolder extends RecyclerView.ViewHolder {

        public final TextView PokemonItemView;
        LinearLayout pokemonGroupParentLayout;
        //final PokemonNameAdapter mAdapter;
        public PokemonGroupViewHolder(@NonNull View itemView, PokemonGroupAdapter pokemonNameAdapter) {
            super(itemView);
            PokemonItemView = itemView.findViewById(R.id.GroupListTextView);
            pokemonGroupParentLayout = itemView.findViewById(R.id.GroupNameParentLayout);
        }

    }
}
