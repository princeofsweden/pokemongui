package com.example.pokdex;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.pokdex.database.PokemonEntities;
import com.example.pokdex.database.PokemonRepository;

import java.util.concurrent.ExecutionException;

public class PokeListViewModel extends AndroidViewModel {
    private PokemonRepository pokemonRepository;

    public PokeListViewModel(@NonNull Application application) {
        super(application);
        this.pokemonRepository = new PokemonRepository(application);
    }

    public void insertPokemon (PokemonEntities entities) {
        pokemonRepository.insert(entities);
    }

    public PokemonEntities[] getAllPokemon() throws ExecutionException, InterruptedException {
        return pokemonRepository.getAllPokemon();
    }

}
