package com.example.pokdex.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface PokemonDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(PokemonEntities pokemonEntities);

    @Query("DELETE FROM poketable")
    void deleteAllPokemon();

    @Query("SELECT * FROM poketable WHERE name = :name")
    PokemonEntities getPokemonById(String name);

    @Query("SELECT * from poketable")
    PokemonEntities[] getAllPokemon();

}