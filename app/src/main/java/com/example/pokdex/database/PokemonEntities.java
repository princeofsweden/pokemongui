package com.example.pokdex.database;

import androidx.annotation.NonNull;

import lombok.Getter;

import lombok.Setter;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Getter
@Setter
@Entity(tableName = "poketable")
public class PokemonEntities {

    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    @ColumnInfo(name = "name")
    private String name;
    @NonNull
    @ColumnInfo(name = "height")
    private String height;
    @NonNull
    @ColumnInfo(name = "weight")
    private String weight;
    @NonNull
    @ColumnInfo(name = "color")
    private String color;
    @NonNull
    @ColumnInfo(name = "shape")
    private String shape;
    @NonNull
    @ColumnInfo(name = "pokedexnumbers")
    private String pokedexnumbers;
    @NonNull
    @ColumnInfo(name = "evolves_from_species")
    private String evolves_from_species;
    @NonNull
    @ColumnInfo(name = " habitat")
    private String  habitat;
    @NonNull
    @ColumnInfo(name = "generations")
    private String generations;
    @NonNull
    @ColumnInfo(name = "flavor_text_entries")
    private String flavor_text_entries;
    @NonNull
    @ColumnInfo(name = "form_descriptions")
    private String  form_descriptions;
    @NonNull
    @ColumnInfo(name ="genera")
    private String  genera;
    @NonNull
    @ColumnInfo(name = "varieties")
    private String  varieties;


    public PokemonEntities() {
    }

    @Override
    public String toString() {
        return "PokemonEntities{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", height='" + height + '\'' +
                ", weight='" + weight + '\'' +
                ", color='" + color + '\'' +
                ", shape='" + shape + '\'' +
                ", pokedexnumbers='" + pokedexnumbers + '\'' +
                ", evolves_from_species='" + evolves_from_species + '\'' +
                ", habitat='" + habitat + '\'' +
                ", generations='" + generations + '\'' +
                ", flavor_text_entries'" + flavor_text_entries + '\'' +
                ", form_descriptions'" + form_descriptions + '\'' +
                ", genera'" + genera + '\'' +
                ", varieties'" + varieties + '\'' +
                '}';
    }
}