package com.example.pokdex;


import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.SearchView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.pokdex.firstpage.PokemonListLoader;
import com.example.pokdex.models.PokemonObjects;
import com.example.pokdex.models.ResultModel;
import com.example.pokdex.recycleView.PokemonGroupAdapter;
import com.example.pokdex.recycleView.PokemonNameAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private PokemonNameAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        ArrayList<String> groups = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            groups.add("Group " + i);
        }
        RecyclerView mRecyclerView = findViewById(R.id.pokemonGroupRecycleView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this, RecyclerView.HORIZONTAL, false));
        PokemonGroupAdapter pokemonGroupAdapter = new PokemonGroupAdapter(groups, MainActivity.this);
        mRecyclerView.setAdapter(pokemonGroupAdapter);
    }

    public void init() {
        //This method is injected as soon as new TaskComplete is entered
        new PokemonListLoader().getAllPokemon(result -> {

            ArrayList<PokemonObjects> pokemonObjects = new ArrayList<>();
            int id = 1;
            for (ResultModel pok : result) {
                PokemonObjects pokemon = new PokemonObjects();
                String name = pok.getName().substring(0, 1).toUpperCase() + pok.getName().substring(1);
                pokemon.setName(name);
                pokemon.setId(id++);
                pokemonObjects.add(pokemon);
            }
            RecyclerView mRecyclerView = findViewById(R.id.pokemonNamesRecycleView);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
            adapter = new PokemonNameAdapter(MainActivity.this, pokemonObjects);
            mRecyclerView.setAdapter(adapter);
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.pok_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (android.widget.SearchView) searchItem.getActionView();

        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;

    }
}

